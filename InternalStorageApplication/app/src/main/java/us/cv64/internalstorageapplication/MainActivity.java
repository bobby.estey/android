// https://www.lucazanini.eu/2016/android/saving-reading-files-internal-storage/
// Example of reading and writing Android internal files
package us.cv64.internalstorageapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import us.cv64.internalstorageapplication.R;

public class MainActivity extends AppCompatActivity {

    private byte[] bytes = new byte[1024];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openInternalFileOutput("bobby", "estey", "message");
        openInternalFileInput("bobby", "estey", "message");
    }

    // writes the internal output file
    private void openInternalFileOutput(String path, String filename, String message) {

        try {

            // https://developer.android.com/reference/android/content/Context#getApplicationContext()
            // returns the reference to the application and all activities
            Context context = getApplicationContext();

            // returns the folder
            String folder = context.getFilesDir().getAbsolutePath() + File.separator + path;

            // folder value:  /data/user/0/us.cv64.internalstorageapplication/files/bobby
            File subFolder = new File(folder);

            // subFolder value:  /data/user/0/us.cv64.internalstorageapplication/files/bobby
            if (!subFolder.exists()) {
                subFolder.mkdirs();
            }

            // create an output stream
            FileOutputStream outputStream = new FileOutputStream(new File(subFolder, filename));

            // write the message to the stream
            outputStream.write(message.getBytes());

            // close the file
            outputStream.close();

        } catch (FileNotFoundException e) {
            Log.e("FileNotFoundException: ", e.toString());
        } catch (IOException e) {
            Log.e("IOException: ", e.toString());
        }
    }

    private void openInternalFileInput(String path, String filename, String message) {

        try {
            Context context = getApplicationContext();
            String folder = context.getFilesDir().getAbsolutePath() + File.separator + path;

            File subFolder = new File(folder);

            // read intput file and create an output stream
            FileInputStream outputStream = new FileInputStream(new File(subFolder, filename));

            // read output stream as bytes
            outputStream.read(bytes);

            // close output stream
            outputStream.close();

            // convert bytes into a String
            String string = new String(bytes);

            // display message as a Toast
            Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG).show();

        } catch (FileNotFoundException e) {
            Log.e("FileNotFoundException: ", e.toString());
        } catch (IOException e) {
            Log.e("IOException: ", e.toString());
        }
    }
}
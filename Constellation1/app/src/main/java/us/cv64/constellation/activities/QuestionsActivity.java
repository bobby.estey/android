package us.cv64.constellation.activities;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import us.cv64.constellation.Constants;
import us.cv64.constellation.R;
import us.cv64.constellation.legal.License;

/**
 * Questions Estey Contacts Program
 */
public class QuestionsActivity extends ParentActivity {

    License license = new License();

    @Override
    protected void onCreate(Bundle savedInstconstellationeState) {
        Log.i(Constants.TAG, "QuestionsActivity.onCreate");
        super.onCreate(savedInstconstellationeState);
        setContentView(R.layout.activity_about);

        TextView textView = (TextView) findViewById(R.id.idQuestionsTextViewScroll);
        textView.setText(license.getLicense());
    }

    // calls the legal content
    public void onClickTextViewScroll(View view) {
        Log.i(Constants.TAG, "QuestionsActivity.onClickTextViewScroll");
        TextView textView = (TextView) findViewById(R.id.idQuestionsTextViewScroll);
        textView.setText(license.getLicense());
        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
    }
}
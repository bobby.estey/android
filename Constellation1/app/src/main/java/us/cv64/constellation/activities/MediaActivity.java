package us.cv64.constellation.activities;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import us.cv64.constellation.Constants;
import us.cv64.constellation.R;

public class MediaActivity extends ParentActivity {

    @Override
    protected void onCreate(Bundle savedInstconstellationeState) {
        super.onCreate(savedInstconstellationeState);

        Log.i(Constants.TAG, "MediaActivity.onCreate");
        setContentView(R.layout.activity_video);

        final VideoView videoView = (VideoView) findViewById(R.id.videoView);

        videoView.setVideoPath(Constants.VIDEO);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                Log.i(Constants.TAG, "MediaActivity - Duration: " + videoView.getDuration());
            }
        });

        videoView.start();
    }
}

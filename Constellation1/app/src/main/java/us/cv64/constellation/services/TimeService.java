package us.cv64.constellation.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeService extends Service {

    public TimeService() {}

    private final IBinder binder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class LocalBinder extends Binder {
        public TimeService getService() {
            return TimeService.this;
        }
    }

    public String getCurrentTime() {
        SimpleDateFormat simpleDataFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
        return simpleDataFormat.format(new Date());
    }
}
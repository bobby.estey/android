package us.cv64.constellation.sql;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import us.cv64.constellation.Constants;

/**
 * Data record for Estey Contacts
 */
public class EsteyRecord implements Parcelable {

    private String record;
    private String id;

    /**
     * Use when reconstructing EsteyRecord object from parcel
     * This will be used only by the 'CREATOR'
     * @param in a parcel to read this object
     */
    public EsteyRecord(Parcel in) {
        this.id = in.readString();
    }

    @Override
    /**
     * Define the kind of object that is being parceled,
     * You can use hashCode() here
     */
    public int describeContents() {
        return 0;
    }

    @Override
    /**
     * Actual object serialization happens here, Write object content
     * to parcel one by one, reading should be done according to this write order
     * @param dest parcel
     * @param flags Additional flags about how the object should be written
     */
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
    }

    /**
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays
     *
     * If you don’t do that, Android framework will through exception
     * Parcelable protocol requires a Parcelable.Creator object called CREATOR
     */
    public static final Parcelable.Creator<EsteyRecord> CREATOR = new Parcelable.Creator<EsteyRecord>() {

        public EsteyRecord createFromParcel(Parcel in) {
            return new EsteyRecord(in);
        }

        public EsteyRecord[] newArray(int size) {
            return new EsteyRecord[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof EsteyRecord) {
            EsteyRecord toCompare = (EsteyRecord) obj;
            return (this.record.equalsIgnoreCase(toCompare.getRecord()));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return (this.getRecord()).hashCode();
    }

    public EsteyRecord(Cursor cursor) {
        Log.i(Constants.TAG, "EsteyRecord.Constructor - Cursor: " + Integer.toString(cursor.getColumnCount()));

        String[] array = new String[Constants.COLUMNS.length];

        for (int i = 0; i < cursor.getColumnCount(); i++) {
            Log.i(Constants.TAG, "[" + i + "]: " + cursor.getString(i));
            array[i] = cursor.getString(i);
        }

        buildRecord(array[0]);
    }

    public EsteyRecord(String id) {

        buildRecord(id);
    }

    private void buildRecord(String id) {

        Log.i(Constants.TAG, "EsteyRecord.buildRecord");

        this.id = id;

        this.record =
                "Identification: " + id;
    }

    public String getRecord() {
        return record;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
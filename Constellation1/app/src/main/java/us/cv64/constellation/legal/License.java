package us.cv64.constellation.legal;

/**
 * license agreement - simple class to keep the lawyers away
 */
public class License {

    String personal = "";
    String license = "";

    public String getLicense() {
        setPersonal();
        setLicense();
        return personal + license;
    }

    private void setPersonal() {

        personal =
                "BETA SOFTWARE - Note the name and dates of the releases\n\n";
    }

    private void setLicense() {
        license =
                "USS Constellation CVA/CV 64 Association Version 0.13\n" +
                        "Copyright © 2016 cv64, LLC\n" +
                        "All Rights Reserved\n" +
                        "\n" +
                        "SOFTWARE LICENSE AGREEMENT\n" +
                        "\n" +
                        "IMPORTANT:  Please read this License Agreement (\"LA\") before using the software.  \n" +
                        "\n" +
                        "The database is owned by USS Constellation CVA/CV 64 Association (who has given permission to Robert W. Estey, Jr. to duplicate) and the software programs (including database access) is owned by cv64, LLC.  Therefore USS Constellation CVA/CV 64 Association and cv64, LLC will be referred to as (\"OWNERS\").\n" +
                        "\n" +
                        "The right to use cv64, LLC Software is a legal agreement between you the (\"CUSTOMER\") and the OWNERS for the software product identified above which may include associated media, printed materials, and ONLINE or electronic documentation (\"SOFTWARE\").  By installing, copying, or otherwise using the SOFTWARE, the CUSTOMER agrees to be bound by the terms of this LA.  If the CUSTOMER does not agree to the terms of this LA, do not install or use the SOFTWARE.  If the CUSTOMER purchased the SOFTWARE, the CUSTOMER may return the SOFTWARE to the place of purchase for a full refund.\n" +
                        "\n" +
                        "COPYRIGHT\n" +
                        "\n" +
                        "The OWNERS own all copies.  Copyright laws and international treaties protect the SOFTWARE. The SOFTWARE is licensed, not sold.  This LA grants the CUSTOMER no rights to use such content.  The OWNERS reserve all rights not expressly granted.\n" +
                        "\n" +
                        "SOFTWARE LICENSE\n" +
                        "\n" +
                        "* Installation and Use.  The LA permits the CUSTOMER to install and use one copy of the SOFTWARE on a single computer running validly licensed copies of the operating system for which the SOFTWARE was designed.  The SOFTWARE is in use on a computer when loaded into temporary memory (e.g. RAM) or installed into permanent memory (e.g. hard disk, CD-ROM, or other storage devices) or a computer.  \n" +
                        "\n" +
                        "* Backup Copies.  The CUSTOMER may make copies of the SOFTWARE for backup purposes.\n" +
                        "\n" +
                        "RIGHTS AND LIMITATIONS\n" +
                        "\n" +
                        "Beta Software\n" +
                        "\n" +
                        "If any component of the SOFTWARE or any of its components is marked \"BETA\" the component of the SOFTWARE constitutes pre-released code and may be changed substantially before commercial release.  You may not use such component in a live operating environment where it may be relied upon to perform in the same manner as a commercially released product or with data that has not been sufficiently backed up.\n" +
                        "\n" +
                        "Maintenance of Copyright Notices\n" +
                        "\n" +
                        "You must not remove or alter any copyright notices on all copies of the SOFTWARE.\n" +
                        "\n" +
                        "Distribution\n" +
                        "\n" +
                        "You may not distribute any copies of the SOFTWARE to third parties.\n" +
                        "\n" +
                        "Prohibition on Reverse Engineering, Decompilation, and Disassembly\n" +
                        "\n" +
                        "You may not reverse engineer, decompile, or disassemble the SOFTWARE, except and only to the extent that applicable law notwithstanding this limitation expressly permits such activity.\n" +
                        "\n" +
                        "Rental\n" +
                        "\n" +
                        "You may not rent, lease, or lend the SOFTWARE.\n" +
                        "\n" +
                        "Termination\n" +
                        "\n" +
                        "The OWNERS may terminate this LA if the CUSTOMER fails to comply with the terms and conditions.  In such event, the CUSTOMER must destroy or return all copies of the SOFTWARE or provide the OWNERS with a certificate of destruction of all SOFTWARE.  If the CUSTOMER modifies the SOFTWARE or includes the SOFTWARE in any other software program, upon termination of this license the CUSTOMER agrees either to remove the Software or any portion thereof from the modified program or return the SOFTWARE to the OWNERS.\n" +
                        "\n" +
                        "Fault Tolerant\n" +
                        "\n" +
                        "The SOFTWARE is not fault-tolerant and is not designed, manufactured or intended for use in conjunction with control equipment in hazardous environments requiring fail-safe performconstellatione.  The OWNERS specifically disclaim any express or implied warranty for High Risk Activities.\n" +
                        "\n" +
                        "NO WARRANTIES\n" +
                        "\n" +
                        "THE OWNERS EXPRESSLY DISCLAIM ANY WARRANTY FOR THE SOFTWARE AND ACCOMPANYING FILES.  THE SOFTWARE PRODUCT AND ANY RELATED DOCUMENTATION IS PROVIDED \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OR MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NONINFRINGEMENT.  THE ENTIRE RISK ARISING OUT OF USE OR PERFORMANCE OF THE SOFTWARE PRODUCT REMAINS WITH THE CUSTOMER.\n" +
                        "\n" +
                        "LIMITATION OF LIABILITY\n" +
                        "\n" +
                        "To the maximum extent permitted by applicable law, in no event shall the OWNERS be liable for any special, incidental, indirect, or consequential damages whatsoever (including, without limitation, damages for loss of business profits, business interruption, loss of business information, or any other pecuniary loss) arising out of the use of or inability to use the SOFTWARE or the provision of or failure to provide Support Services, even if the OWNERS have been advised of the possibility of such damages. In any case, the OWNERS entire liability under any provision of this LA shall be limited to the greater of the amount actually paid by the CUSTOMER for the SOFTWARE or US $3.00;\n" +
                        "\n" +
                        "This software is not designed or intended for use in on-line control of aircraft, air traffic, aircraft navigation or aircraft communications; or in\n" +
                        "the design, construction, operation or maintenconstellatione of any nuclear\n" +
                        "facility. Licensee represents and warrants that it will not use or\n" +
                        "redistribute the Software for such purposes.\n" +
                        "\n" +
                        "U.S.  Government Restricted Rights\n" +
                        "\n" +
                        "The SOFTWARE and documentation are provided with restricted rights.  Use, duplication, or disclosure by the Government is subject to the Federal Acquisition Regulations (FAR) in paragraph 252.227-7013c (1) if the Software is supplied to the Department of Defense, or subject to Federal Acquisition Regulations in paragraph 52.227-19c (2) if supplied to any other unit or agency of the U.S. Government. \n" +
                        "\n" +
                        "MISCELLANEOUS\n" +
                        "\n" +
                        "AUTHOR (To place additional orders, please contact)\n" +
                        "\n" +
                        "USS Constellation CVA/CV 64 Association\n" +
                        "ussconstellation.org\n" +
                        "\n" +
                        "SOFTWARE DEVELOPER (Should you have any questions concerning this LA, or would like to contract software, please contact)\n" +
                        "\n" +
                        "cv64, LLC\n" +
                        "bobbycv64@yahoo.com\n" +
                        "757.846.4352";
    }
}

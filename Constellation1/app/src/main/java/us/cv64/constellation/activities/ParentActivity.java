package us.cv64.constellation.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import us.cv64.constellation.Constants;
import us.cv64.constellation.R;
import us.cv64.constellation.activities.util.ActionBarUtil;
import us.cv64.constellation.sql.DatabaseHandler;

public class ParentActivity extends AppCompatActivity {

    protected DatabaseHandler databaseHandler = null;
    protected String csvRecords = "";

    /**
     * get a database handler object
     */
    protected DatabaseHandler getDatabaseHandler() {
        Log.i(Constants.TAG, "ParentActivity.getDatabaseHandler");

        if (null == databaseHandler) {
            databaseHandler = DatabaseHandler.getInstance(this);
        }

        return databaseHandler;
    }

    @Override
    /**
     * close out all objects
     */
    protected void onDestroy() {
        Log.i(Constants.TAG, "ParentActivity.onDestroy");

        super.onDestroy();

        if (null != databaseHandler) {
            databaseHandler.close();
            databaseHandler = null;
        }

        csvRecords = "";
    }

    @Override
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(Constants.TAG, "ParentActivity.onOptionsItemSelected");
        Intent intent = null;
        String actionBarString = new ActionBarUtil(item).getActionBarString();
        Toast.makeText(this, actionBarString, Toast.LENGTH_SHORT).show();

        // go to home page
        if (actionBarString.equals("home")) {
            intent = new Intent(this, MainActivity.class);

            // do not navigate to another page
        } else if(actionBarString.equals(Constants.CONSTELLATION_LINKS)) {
            return false;

            // go to new activity page
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(new ActionBarUtil(item).getActionBarLink()));
        }
        startActivity(intent);

        return super.onOptionsItemSelected(item);
    }

    @Override
    // Inflate the menu; this adds items to the action bar if it is present.
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(Constants.TAG, "ParentActivity.onCreateOptionsMenu");

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}

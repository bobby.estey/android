package us.cv64.constellation.activities.util;

import android.util.Log;
import android.view.MenuItem;

import us.cv64.constellation.Constants;
import us.cv64.constellation.R;

public class ActionBarUtil {

    private String actionBarString = Constants.CONSTELLATION_LINKS;
    private String actionBarLink = Constants.CONSTELLATION_LINKS;

    public ActionBarUtil(MenuItem item) {
        Log.i(Constants.TAG, "ActionBarUtil Constructor");

        try {

            int id = item.getItemId();

            // return String from Action Bar
            if (id == R.id.home) {
                actionBarString = "home";
                actionBarLink = "home";
            } else if (id == R.id.cv64logo) {
                actionBarString = Constants.CV64;
                actionBarLink = Constants.CV64;
            } else if (id == R.id.constellationFacebook) {
                actionBarString = Constants.CONSTELLATION_FACEBOOK_TITLE;
                actionBarLink = Constants.CONSTELLATION_FACEBOOK_LINK;
            } else if (id == R.id.version) {
                actionBarString = Constants.VERSION;
                actionBarLink = Constants.CV64_VERSION;
            } else {
                actionBarString = Constants.CONSTELLATION_LINKS;
                actionBarLink = Constants.CONSTELLATION_LINKS;
            }
        } catch (Exception exception) {
            exception.getMessage();
            exception.printStackTrace();
        }

        Log.i(Constants.TAG, "ActionBarUtil Constructor - actionBarString: " + actionBarString);
        Log.i(Constants.TAG, "ActionBarUtil Constructor - actionBarLink:   " + actionBarLink);
    }

    public String getActionBarString() {
        Log.i(Constants.TAG, "ActionBarUtil.getActionBarString: " + actionBarString);
        return actionBarString;
    }

    public String getActionBarLink() {
        Log.i(Constants.TAG, "ActionBarUtil.getActionBarLink: " + actionBarLink);
        return actionBarLink;
    }
}

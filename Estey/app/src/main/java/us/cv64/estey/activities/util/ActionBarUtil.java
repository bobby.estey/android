package us.cv64.estey.activities.util;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import us.cv64.estey.Constants;
import us.cv64.estey.R;

public class ActionBarUtil {

    private String actionBarString = Constants.CV64;
    private String actionBarLink = Constants.CV64;

    public ActionBarUtil(MenuItem item) {
        Log.i(Constants.TAG, "ActionBarUtil.getActionBarString");

        try {

            int id = item.getItemId();

            // return String from Action Bar
            if (id == R.id.home) {
                actionBarString = "home";
                actionBarLink = "home";
            } else if (id == R.id.cv64logo) {
                actionBarString = Constants.CV64;
                actionBarLink = Constants.CV64;
            } else if (id == R.id.esteyMI) {
                actionBarString = "Estey, MI";
                actionBarLink = Constants.ESTEY_MI;
            } else if (id == R.id.esteyCityNM) {
                actionBarString = "Estey City, NM";
                actionBarLink = Constants.ESTEY_CITY_NM;
            } else if (id == R.id.version) {
                actionBarString = Constants.VERSION;
                actionBarLink = Constants.CV64_VERSION;
            }
        } catch (Exception exception) {
            exception.getMessage();
            exception.printStackTrace();
        }
    }

    public String getActionBarString() {
        return actionBarString;
    }

    public String getActionBarLink() {
        return actionBarLink;
    }
}

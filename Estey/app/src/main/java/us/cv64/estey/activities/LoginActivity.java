package us.cv64.estey.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import us.cv64.estey.Constants;
import us.cv64.estey.R;

/**
 * Login Activity Class
 */
public class LoginActivity extends AppCompatActivity {

    private EditText username = null;
    private EditText password = null;
    private Button login = null;
    private TextView loginLockedTV = null;
    private TextView attemptsLeftTV = null;
    private TextView numberOfRemainingLoginAttemptsTV = null;
    private int numberOfRemainingLoginAttempts = Constants.LOGIN_ATTEMPTS;

    @Override
    /**
     * initializes login activity
     */
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.TAG, "LoginActivity.onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupVariables();
    }

    /**
     * authenticates the login and navigates to the Main Activity on successful login
     *
     * @param view
     */
    public void authenticateLogin(View view) {
        Log.i(Constants.TAG, "LoginActivity.authenticateLogin");

        Intent intent = new Intent(this, MainActivity.class);

        // debug - start activity
        if (Constants.DEBUG) {

            // start the Main activity
            startActivity(intent);

        // force the user to login
        } else {

            if (username.getText().toString().equals(Constants.USERNAME) &&
                    password.getText().toString().equals(Constants.PASSWORD)) {

                Toast.makeText(getApplicationContext(), "Welcome " + username.getText(),
                        Toast.LENGTH_SHORT).show();

                intent = new Intent(this, MainActivity.class);

                // start the Main activity
                startActivity(intent);

            } else {
                Toast.makeText(getApplicationContext(), "Invalid Account",
                        Toast.LENGTH_SHORT).show();
                numberOfRemainingLoginAttempts--;
                attemptsLeftTV.setVisibility(View.VISIBLE);
                numberOfRemainingLoginAttemptsTV.setVisibility(View.VISIBLE);
                numberOfRemainingLoginAttemptsTV.setText(Integer.toString(numberOfRemainingLoginAttempts));

                if (numberOfRemainingLoginAttempts == 0) {
                    login.setEnabled(false);
                    loginLockedTV.setVisibility(View.VISIBLE);
                    loginLockedTV.setBackgroundColor(Color.RED);
                    loginLockedTV.setText("LOGIN LOCKED!!!");
                }
            }
        }
    }

    /**
     * set up login variables
     */
    private void setupVariables() {
        Log.i(Constants.TAG, "LoginActivity.setupVariables");

        username = (EditText) findViewById(R.id.usernameET);
        password = (EditText) findViewById(R.id.passwordET);
        login = (Button) findViewById(R.id.loginBtn);
        loginLockedTV = (TextView) findViewById(R.id.loginLockedTV);
        attemptsLeftTV = (TextView) findViewById(R.id.attemptsLeftTV);
        numberOfRemainingLoginAttemptsTV = (TextView) findViewById(R.id.numberOfRemainingLoginAttemptsTV);
        numberOfRemainingLoginAttemptsTV.setText(Integer.toString(numberOfRemainingLoginAttempts));
    }
}
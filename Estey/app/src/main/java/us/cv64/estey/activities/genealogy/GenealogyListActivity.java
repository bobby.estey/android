package us.cv64.estey.activities.genealogy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import us.cv64.estey.Constants;
import us.cv64.estey.R;
import us.cv64.estey.activities.ParentActivity;
import us.cv64.estey.sql.EsteyRecord;

/**
 * Genealogy List Activity - lists the children selections
 */
public class GenealogyListActivity extends ParentActivity {

    private ArrayList<String> items = new ArrayList<String>();
    private ListAdapter listAdapter = null;
    private ListView listView = null;
    private String childId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.TAG, "GenealogyListActivity.onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genealogy_list);

        // get data from GenealogyActivity
        Bundle bundle = getIntent().getExtras();

        if (bundle == null) {
            return;
        }

        String genealogy = bundle.getString("genealogy");
        String parentName = bundle.getString("parentName");
        int esteyRecordsSize = bundle.getInt("esteyRecordsSize");

        Button button = (Button) findViewById(R.id.idGenealogyListButton);
        button.setText(genealogy);
        TextView textView = (TextView) findViewById(R.id.idGenealogyListTextViewScroll);
        textView.setText(parentName);


        for (int i = 0; i < esteyRecordsSize; i++) {
            EsteyRecord esteyRecord = bundle.getParcelable("us.cv64.estey.sql.EsteyRecord" + i);
            items.add(esteyRecord.getGenealogy() + "~" + esteyRecord.getName());
        }

        listAdapter = new GenealogyListAdapter(this, items);

        listView = (ListView) findViewById(R.id.idListView);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    // get the item from the list that was clicked
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String item = String.valueOf(parent.getItemAtPosition(position));
                        childId = String.valueOf(parent.getItemAtPosition(position));
                        Toast.makeText(GenealogyListActivity.this, item, Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    // calls the GenealogyActivity activity
    public void onGenealogyListClick(View view) {
        Log.i(Constants.TAG, "GenealogyListActivity.onClick");

        Button button = button = (Button) view.findViewById(R.id.idGenealogyListButton);

        return2caller(button);
    }

    // calls the GenealogyActivity activity
    public void onGenealogyListRowClick(View view) {
        Log.i(Constants.TAG, "GenealogyListActivity.onClick");

        Button button = (Button) view.findViewById(R.id.idGenealogyListRowButton);

        return2caller(button);
    }

    private void return2caller(Button button) {

        childId = button.getText().toString();

        Intent intent = new Intent();
        intent.putExtra("childId",childId);

        setResult(Constants.ACTIVITY_REQUEST_CODE,intent);

        finish();
    }
}
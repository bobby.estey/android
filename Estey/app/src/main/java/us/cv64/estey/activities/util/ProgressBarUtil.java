package us.cv64.estey.activities.util;

import android.app.ProgressDialog;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import us.cv64.estey.Constants;

public class ProgressBarUtil {

    private ProgressDialog progressBar = null;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    int recordNumber = 0;

    private long fileSize = 0;

    public void onClickb(View view) {

        // prepare for a progress bar dialog
        progressBar = new ProgressDialog(view.getContext());
        progressBar.setCancelable(true);
        progressBar.setMessage("File downloading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();

        //reset progress bar status
        progressBarStatus = 0;

        //reset filesize
        fileSize = 0;

        new Thread(new Runnable() {
            public void run() {

                Log.i(Constants.TAG, "DatabaseHandler.gggggggggggggggggggg: " + progressBarStatus);

                while (progressBarStatus < 100) {

                    // process some tasks
                    progressBarStatus = doSomeTasks();
                    //progressBarStatus = recordNumber / 44;
                    Log.i(Constants.TAG, "DatabaseHandler.kkkkkkkkkkkkkkkkkkkkkkkkk: " + progressBarStatus);
                    // your computer is too fast, sleep 1 second
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // Update the progress bar
                    progressBarHandler.post(new Runnable() {
                        public void run() {
                            Log.i(Constants.TAG, "DatabaseHandler.bbbbbbbbbbbbbbbbbbbbbbbbbb: " + progressBarStatus);
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }

                // ok, file is downloaded,
                if (progressBarStatus >= 100) {

                    // sleep 2 seconds, so that you can see the 100%
                    try {
                        Log.i(Constants.TAG, "DatabaseHandler.eeeeeeeeeeeeeeeeeee: " + progressBarStatus);
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // close the progress bar dialog
                    progressBar.dismiss();
                }
            }
        }).start();

    }

    // file download simulator... a really simple
    public int doSomeTasks() {


        while (fileSize <= 1000000) {

            fileSize++;

            if (fileSize == 100000) {
                return 10;
            } else if (fileSize == 200000) {
                return 20;
            } else if (fileSize == 300000) {
                return 30;
            }
            // ...add your own

        }

        return 100;

    }
}
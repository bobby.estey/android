package us.cv64.estey.activities.genealogy;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import us.cv64.estey.R;
import us.cv64.estey.Constants;
import us.cv64.estey.activities.ParentActivity;
import us.cv64.estey.sql.EsteyRecord;

/**
 * Genealogy Activity - displays information about an individual and contains navigation aids up and
 * down the Genealogy tree
 */
public class GenealogyActivity extends ParentActivity {

    private String identification = Constants.IDENTIFICATION_ROOT;
    private String index = Constants.GENEALOGY_INDEX_ROOT;
    private String genealogy = Constants.GENEALOGY_PARENT_ROOT;
    private String parentName = "";
    private String childId = Constants.GENEALOGY_CHILD_ROOT;
    private EsteyRecord esteyRecord = null;

    @Override
    /**
     * populate genealogy with default ancestor
     */
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.TAG, "GenealogyActivity.onCreate");

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_genealogy);

            // get data from Main Activity
            Bundle mainActivityBundle = getIntent().getExtras();

            if (mainActivityBundle == null) {
                return;
            }

            genealogy = mainActivityBundle.getString("genealogy");
            index = mainActivityBundle.getString("index");
            identification = mainActivityBundle.getString("identification");

            recalibrate();

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * controls the scrolling of the text view
     * @param view
     */
    public void onClickTextViewScroll(View view) {
        Log.i(Constants.TAG, "GenealogyActivity.onClickTextViewScroll.onClickCV64");

        TextView textView = (TextView) findViewById(R.id.idGenealogyTextViewScroll);
        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
    }

    /**
     * get the parent record from the database
     *
     * @param view
     */
    public void onClickSearchParent(View view) {
        Log.i(Constants.TAG, "GenealogyActivity.onClickSearchParent");

        String currentGenealogy = genealogy;

        try {
            genealogy = genealogy.substring(0, genealogy.length() - 1);
            recalibrate();

        } catch (Exception exception) {
            genealogy = currentGenealogy;
            Toast.makeText(getApplicationContext(), "No Records Found", Toast.LENGTH_SHORT).show();
            exception.printStackTrace();
        }
    }

    /**
     * get children records from the database
     * @param view
     */
    public void onClickSearchChildren(View view) {
        Log.i(Constants.TAG, "GenealogyActivity.onClickSearchChildren");

        ArrayList<EsteyRecord> esteyRecords = databaseHandler.getRecords(genealogy);

        if (null == esteyRecords) {
            Toast.makeText(getApplicationContext(), "No Records Found", Toast.LENGTH_SHORT).show();
            return;
        }

        parentName = databaseHandler.getRecord("GENEALOGY", genealogy).getName();

        Intent intent = new Intent(this, GenealogyListActivity.class);

        // pass parameters (key / value) to the GenealogyListActivity through extras
        intent.putExtra("genealogy", genealogy);
        intent.putExtra("parentName", parentName);

        int esteyRecordsSize = esteyRecords.size();
        intent.putExtra("esteyRecordsSize", esteyRecordsSize);

        for (int i = 0; i < esteyRecordsSize; i++) {
            EsteyRecord esteyRecord = esteyRecords.get(i);
            intent.putExtra("us.cv64.estey.sql.EsteyRecord" + i, esteyRecord);
        }

        startActivityForResult(intent, Constants.ACTIVITY_REQUEST_CODE);
    }

    /**
     * get records from the database
     *
     * @param view
     */
    public void onClickSearch(View view) {
        Log.i(Constants.TAG, "GenealogyActivity.onClickSearch");

        Intent intent = new Intent(this, GenealogySearchActivity.class);

        // start the Genealogy Search activity
        startActivity(intent);
    }

    /**
     * get records from the database
     *
     * @param view
     */
    public void onClickPhotos(View view) {
        Log.i(Constants.TAG, "GenealogyActivity.onClickPhotos");

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.PHOTOS));
        startActivity(intent);
    }

    @Override
    /**
     * Call Back method  to get the Message form other Activity
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.i(Constants.TAG, "GenealogyActivity.onActivityResult");

        try {
            super.onActivityResult(requestCode, resultCode, data);

            // check if the request code is same as what is passed
            if (requestCode == Constants.ACTIVITY_REQUEST_CODE) {
                if (null == data) {
                    childId = Constants.GENEALOGY_PARENT_ROOT;
                } else {
                    childId = data.getStringExtra("childId");  // fetch the message String
                }

                genealogy = childId;

                esteyRecord = databaseHandler.getRecord("GENEALOGY", childId);

                populateGenealogyActivity();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * populate the genealogy activity
     */
    private void populateGenealogyActivity() {
        Log.i(Constants.TAG, "GenealogyActivity.populateGenealogyActivity");

        TextView textViewIdentification = (TextView) findViewById(R.id.idIdentificationValue);
        textViewIdentification.setText(esteyRecord.getId());

        TextView indexValue = (TextView) findViewById(R.id.idIndexValue);
        indexValue.setText(esteyRecord.getGenealogyIndex());

        TextView genealogyValue = (TextView) findViewById(R.id.idGenealogyValue);
        genealogyValue.setText(esteyRecord.getGenealogy());

        TextView nameValue = (TextView) findViewById(R.id.idNameValue);
        nameValue.setText(esteyRecord.getName());

        TextView dateOfBirthValue = (TextView) findViewById(R.id.idDateOfBirthValue);
        dateOfBirthValue.setText(esteyRecord.getDateOfBirth());

        TextView dateOfDeathValue = (TextView) findViewById(R.id.idDateOfDeathValue);
        dateOfDeathValue.setText(esteyRecord.getDateOfDeath());

        TextView spouceNameValue = (TextView) findViewById(R.id.idSpouceNameValue);
        spouceNameValue.setText(esteyRecord.getSpouseName());

        TextView spouceDOBValue = (TextView) findViewById(R.id.idSpouceDOBValue);
        spouceDOBValue.setText(esteyRecord.getSpouseDateOfBirth());

        TextView spouceDODValue = (TextView) findViewById(R.id.idSpouceDODValue);
        spouceDODValue.setText(esteyRecord.getSpouseDateOfDeath());

        TextView textView = (TextView) findViewById(R.id.idGenealogyTextViewScroll);
        textView.setText(esteyRecord.getMisc());
    }

    /**
     * recalibrates - resets parent and child records
     */
    private void recalibrate() throws Exception {
        Log.i(Constants.TAG, "GenealogyActivity.recalibrate");

        try {
            if (null == databaseHandler) {
                databaseHandler = getDatabaseHandler();
            }

            if (!genealogy.isEmpty()) {
                esteyRecord = databaseHandler.getRecord("GENEALOGY", genealogy);
                index = esteyRecord.getGenealogyIndex();
                identification = esteyRecord.getId();
            } else if (!index.isEmpty()) {
                esteyRecord = databaseHandler.getRecord("GENEALOGY_INDEX", index);
                genealogy = esteyRecord.getGenealogy();
                identification = esteyRecord.getId();
            } else if (!identification.isEmpty()) {
                esteyRecord = databaseHandler.getRecord("IDENTIFICATION", identification);
                genealogy = esteyRecord.getGenealogy();
                index = esteyRecord.getGenealogyIndex();
            } else {
                esteyRecord = databaseHandler.getRecord("GENEALOGY", Constants.GENEALOGY_PARENT_ROOT);
                genealogy = esteyRecord.getGenealogy();
                index = esteyRecord.getGenealogyIndex();
                identification = esteyRecord.getId();
            }

            childId = genealogy + "A";

            parentName = esteyRecord.getName();

            populateGenealogyActivity();
        } catch (Exception exception) {
            throw exception;
        }
    }
}
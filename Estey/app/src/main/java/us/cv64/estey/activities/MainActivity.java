package us.cv64.estey.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;

import us.cv64.estey.Constants;
import us.cv64.estey.R;
import us.cv64.estey.activities.genealogy.GenealogyActivity;
/**
 * Main Screen for the entire Application
 */
public class MainActivity extends ParentActivity {

    final private Context context = this;
    private ImageButton button = null;
    private AlertDialog.Builder alertDialogBuilder = null;
    private AlertDialog alertDialog = null;

    private String index = Constants.GENEALOGY_INDEX_ROOT;
    private String genealogy = Constants.GENEALOGY_PARENT_ROOT;

    @Override
    // launch the Intent Service when application starts
    //Intent intent = new Intent(this, EsteyIntentService.class);
    //startService(intent);
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.TAG, "MainActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /** keep this for future use somewhere else - gets the view's id
            final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
            .findViewById(android.R.id.content)).getChildAt(0);
        */
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * calls the coat of arms activity
     * @param view
     */
    public void onClickCoatOfArmsActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickCoatOfArmsActivity");

        //Intent intent = new Intent(this, CoatOfArmsActivity.class);
        //startActivity(intent);
    }

    /**
     * calls the genealogy activity
     *
     * @param view
     */
    public void onClickGenealogyActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickGenealogyActivity");

        setAlertDialogBuilder();
        callGenealogyActivity(view);
    }

    /**
     * calls the CSV Text activity
     *
     * @param view
     */
    public void onClickCSVTextActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickCSVTextActivity");

        Intent intent = new Intent(this, CSVTextActivity.class);

        if ((null == csvRecords) || csvRecords.isEmpty()) {
            csvRecords = getCSVRecords();
        }

        // pass the key / value to the About Activity
        intent.putExtra("csvRecords", csvRecords);

        // start the About activity
        startActivity(intent);
    }

    /**
     * calls the coat of arms activity
     * @param view
     */
    public void onClickVideoActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickVideoActivity");

        Intent intent = new Intent(this, VideoActivity.class);
        startActivity(intent);
    }

    /**
     *     calls the About activity
     */
    public void onClickAboutActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickAboutActivity");

        Intent intent = new Intent(this, AboutActivity.class);

        final TextView textView = (TextView) findViewById(R.id.aboutTextView);
        String userMessage = textView.getText().toString();

        // pass the key / value to the About Activity
        intent.putExtra("userMessage", userMessage);

        Log.i(Constants.TAG, "MainActivity.onClickAboutActivity.userMessage: " + userMessage);

        // start the About activity
        startActivity(intent);
    }

    /**
     * set up the Alert Dialog Builder for the database
     */
    private void setAlertDialogBuilder() {
        Log.i(Constants.TAG, "MainActivity.setAlertDialogBuilder");

        final TableLayout    tableLayout = (TableLayout) findViewById(R.id.tableLayout1);
        button = (ImageButton) findViewById(R.id.genealogyImageButton);

        // add button listener
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (null != databaseHandler) return;

                alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("Load Database");


                // set dialog message
                alertDialogBuilder
                        .setMessage("Loading Database...")
                        .setCancelable(false)
                        .setPositiveButton("Press to Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, load the database
                                getDatabaseHandler();
                                dialog.cancel();
                                onClickGenealogyActivity(tableLayout);
                            }
                        })
                ;

                // create alert dialog
                alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    /**
     * call the genealogy activity
     * @param view
     */
    private void callGenealogyActivity(View view) {

        Intent intent = new Intent(this, GenealogyActivity.class);

        switch (view.getId()) {
            case R.id.rolandImageButton:
                System.out.println("Roland");
                genealogy = Constants.ROLAND;
                break;
            case R.id.ethelImageButton:
                System.out.println("Ethel");
                genealogy = Constants.ETHEL;
                break;
            case R.id.johnImageButton:
                System.out.println("John");
                genealogy = Constants.JOHN;
                break;
            case R.id.josephImageButton:
                System.out.println("Joseph");
                genealogy = Constants.JOSEPH;
                break;
            case R.id.margaretImageButton:
                System.out.println("Margaret");
                genealogy = Constants.MARGARET;
                break;
            case R.id.robertImageButton:
                System.out.println("Robert");
                genealogy = Constants.ROBERT;
                break;
            default:
                System.out.println("Default");
                genealogy = Constants.GENEALOGY_PARENT_ROOT;
                break;
        }

        // pass the key / value to the Genealogy Activity
        intent.putExtra("index", index);
        intent.putExtra("genealogy", genealogy);

        Log.i(Constants.TAG, "MainActivity.onClickGenealogyActivity.genealogy: " + genealogy);

        // start the Genealogy activity
        startActivity(intent);
    }
}
package us.cv64.estey;

public class Constants {
    public static final boolean DEBUG = false;  // true - bypass password login
    public static final boolean DATABASE_RESET = false;  // true - rebuild database
    public static final int DATABASE_OLD_VERSION = 1;
    public static final int DATABASE_NEW_VERSION = 1;
    public static final String VERSION = "v20151111 (Jeanne)";

    public static final int LOGIN_ATTEMPTS = 3;
    public static final String USERNAME = "estey";
    public static final String PASSWORD = "roland";

    public static final int ACTIVITY_REQUEST_CODE = 1;
    public static final String TAG = "us.cv64.estey";
    public static final String IDENTIFICATION_ROOT = "23";
    public static final String GENEALOGY_INDEX_ROOT = "8";
    public static final String GENEALOGY_PARENT_ROOT = "G";  // root of genealogy tree
    public static final String GENEALOGY_CHILD_ROOT = "GA";

    public static final String NEBRASKA_WHITE = "#f5f1e7"; // Nebraska WHITE
    public static final String NEBRASKA_RED = "#d00000"; // Nebraska RED

    public static final String DELIMETER = "~"; // database delimeter
    public static final String CSV_FILE = "estey2002Classified.csv";  // located in project assets directory
    public static final int    csvStartIndex = 5000; // id for start range of csv file
    public static final int    csvEndIndex = 5049; // id for end rand of csv file
    public static final int    csvRecordRange = 50; // the range of records
    public static final String DATABASE_NAME = "estey.database";
    public static final String TABLE_NAME = "estey";

    public static final String ROLAND = "5000"; // "1369";
    public static final String ETHEL = "5000"; // "1369";
    public static final String JOHN = "5000A"; // "1852";
    public static final String JOSEPH = "5000B"; // 1855";
    public static final String MARGARET = "5000C"; // "1853";
    public static final String ROBERT = "5000D"; // "1854";

    public static final String CV64 = "http://www.cv64.us/cv64/";
    public static final String CV64_VERSION = "http://www.cv64.us/cv64/genealogy/version";
    public static final String ESTEY_MI = "http://www.cv64.us/cv64/genealogy/esteyMI";
    public static final String ESTEY_CITY_NM = "http://www.cv64.us/cv64/genealogy/esteyCityNM";
    public static final String PHOTOS = "http://www.cv64.us/cv64/genealogy/photos";
    public static final String VIDEO = "http://www.cv64.us/cv64/genealogy/version/jeanne/jeanne.mp4";

    public static final String[] COLUMNS = {
            "IDENTIFICATION", "GENEALOGY_INDEX", "GENEALOGY", "NAME", "DATE_OF_BIRTH", "DATE_OF_DEATH",
            "SPOUSE", "SPOUSE_DOB", "SPOUSE_DOD",
            "MISC01", "MISC02", "MISC03", "MISC04", "MISC05", "MISC06", "MISC07", "MISC08", "MISC09", "MISC10"};
}

package us.cv64.estey.activities.genealogy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import us.cv64.estey.R;

/**
 * The data model for the genealogy list
 */
class GenealogyListAdapter extends ArrayAdapter<String> {

    String item = "";  // element from array list
    String genealogy = "";  // the actual genealogy code, e.g. GABAHAC
    String name = "";  // persons name

    /**
     * Instantiates the data model for the genealogy list
     * @param context - container for the data
     * @param items - data for the model
     */
    public GenealogyListAdapter(Context context, ArrayList<String> items) {

        // get the custom_row.xml file
        super(context, R.layout.activity_genealogy_list_row, items);
    }

    @Override
    /**
     * populates the container with data
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        // context - map to the GUI / front end (display)
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.activity_genealogy_list_row, parent, false);

        // retrieve one item from items ArrayList and populate widgets with data
        item = getItem(position);
        ImageView imageView = (ImageView) customView.findViewById(R.id.idGenealogyListRowImageView);
        Button button = (Button) customView.findViewById(R.id.idGenealogyListRowButton);
        TextView textView = (TextView) customView.findViewById(R.id.idGenealogyListRowTextView);

        parseString();
        imageView.setImageResource(R.mipmap.icon_greenball);
        button.setText(genealogy);
        textView.setText(name);

        return customView;
    }

    /**
     * parses out the data from the string
     */
    private void parseString() {

        int prefix = item.indexOf("~");
        int suffix = item.indexOf("~")+1;
        genealogy = item.substring(0,prefix);
        name = item.substring(suffix,item.length());
    }
}
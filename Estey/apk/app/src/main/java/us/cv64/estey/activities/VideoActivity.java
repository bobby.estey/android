package us.cv64.estey.activities;

import android.os.Bundle;
import android.widget.VideoView;
import android.widget.MediaController;
import android.util.Log;
import android.media.MediaPlayer;

import us.cv64.estey.Constants;
import us.cv64.estey.R;

public class VideoActivity extends ParentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(Constants.TAG, "VideoActivity.onCreate");
        setContentView(R.layout.activity_video);

        final VideoView videoView = (VideoView) findViewById(R.id.videoView);

        videoView.setVideoPath(Constants.VIDEO);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                Log.i(Constants.TAG, "VideoActivity - Duration: " + videoView.getDuration());
            }
        });

        videoView.start();
    }
}

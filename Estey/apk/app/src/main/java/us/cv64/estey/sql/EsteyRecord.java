package us.cv64.estey.sql;

import android.database.Cursor;
import android.util.Log;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

import us.cv64.estey.Constants;

/**
 * Data record for Estey Genealogy
 */
public class EsteyRecord implements Parcelable {

    private String record;
    private String id;
    private String genealogyIndex;
    private String genealogy;
    private String name;
    private String dateOfBirth;
    private String dateOfDeath;
    private String spouseName;
    private String spouseDateOfBirth;
    private String spouseDateOfDeath;
    private String misc01;
    private String misc02;
    private String misc03;
    private String misc04;
    private String misc05;
    private String misc06;
    private String misc07;
    private String misc08;
    private String misc09;
    private String misc10;

    /**
     * Use when reconstructing EsteyRecord object from parcel
     * This will be used only by the 'CREATOR'
     * @param in a parcel to read this object
     */
    public EsteyRecord(Parcel in) {
        this.id = in.readString();
        this.genealogyIndex = in.readString();
        this.genealogy = in.readString();
        this.name = in.readString();
        this.dateOfBirth = in.readString();
        this.dateOfDeath = in.readString();
        this.spouseName = in.readString();
        this.spouseDateOfBirth = in.readString();
        this.spouseDateOfDeath = in.readString();
        this.misc01 = in.readString();
        this.misc02 = in.readString();
        this.misc03 = in.readString();
        this.misc04 = in.readString();
        this.misc05 = in.readString();
        this.misc06 = in.readString();
        this.misc07 = in.readString();
        this.misc08 = in.readString();
        this.misc09 = in.readString();
        this.misc10 = in.readString();
    }

    @Override
    /**
     * Define the kind of object that is being parceled,
     * You can use hashCode() here
     */
    public int describeContents() {
        return 0;
    }

    @Override
    /**
     * Actual object serialization happens here, Write object content
     * to parcel one by one, reading should be done according to this write order
     * @param dest parcel
     * @param flags Additional flags about how the object should be written
     */
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(genealogyIndex);
        dest.writeString(genealogy);
        dest.writeString(name);
        dest.writeString(dateOfBirth);
        dest.writeString(dateOfDeath);
        dest.writeString(spouseName);
        dest.writeString(spouseDateOfBirth);
        dest.writeString(spouseDateOfDeath);
        dest.writeString(misc01);
        dest.writeString(misc02);
        dest.writeString(misc03);
        dest.writeString(misc04);
        dest.writeString(misc05);
        dest.writeString(misc06);
        dest.writeString(misc07);
        dest.writeString(misc08);
        dest.writeString(misc09);
        dest.writeString(misc10);
    }

    /**
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays
     *
     * If you don’t do that, Android framework will through exception
     * Parcelable protocol requires a Parcelable.Creator object called CREATOR
     */
    public static final Parcelable.Creator<EsteyRecord> CREATOR = new Parcelable.Creator<EsteyRecord>() {

        public EsteyRecord createFromParcel(Parcel in) {
            return new EsteyRecord(in);
        }

        public EsteyRecord[] newArray(int size) {
            return new EsteyRecord[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof EsteyRecord) {
            EsteyRecord toCompare = (EsteyRecord) obj;
            return (this.record.equalsIgnoreCase(toCompare.getRecord()));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return (this.getRecord()).hashCode();
    }

    public EsteyRecord(Cursor cursor) {
        Log.i(Constants.TAG, "EsteyRecord.Constructor - Cursor: " + Integer.toString(cursor.getColumnCount()));

        String[] array = new String[Constants.COLUMNS.length];

        for (int i = 0; i < cursor.getColumnCount(); i++) {
            Log.i(Constants.TAG, "[" + i + "]: " + cursor.getString(i));
            array[i] = cursor.getString(i);
        }

        buildRecord(array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7], array[8], array[9], array[10],
                array[11], array[12], array[13], array[14], array[15], array[16], array[17], array[18]);
    }

    public EsteyRecord(String id, String genealogyIndex, String genealogy,
                      String name, String dateOfBirth, String dateOfDeath,
                      String spouseName, String spouseDateOfBirth, String spouseDateOfDeath,
                      String misc01, String misc02, String misc03, String misc04, String misc05,
                String misc06, String misc07, String misc08, String misc09, String misc10) {

            buildRecord(id, genealogyIndex, genealogy, name, dateOfBirth, dateOfDeath,
                    spouseName, spouseDateOfBirth, spouseDateOfDeath,
                    misc01, misc02, misc03, misc04, misc05, misc06, misc07, misc08, misc09, misc10);
    }

    private void buildRecord(String id, String genealogyIndex, String genealogy,
                             String name, String dateOfBirth, String dateOfDeath,
                             String spouseName, String spouseDateOfBirth, String spouseDateOfDeath,
                             String misc01, String misc02, String misc03, String misc04, String misc05,
                             String misc06, String misc07, String misc08, String misc09, String misc10) {

        Log.i(Constants.TAG, "EsteyRecord.buildRecord");

        this.id = id;
        this.genealogyIndex = genealogyIndex;
        this.genealogy = genealogy;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.dateOfDeath = dateOfDeath;
        this.spouseName = spouseName;
        this.spouseDateOfBirth = spouseDateOfBirth;
        this.spouseDateOfDeath = spouseDateOfDeath;
        this.misc01 = misc01;
        this.misc02 = misc02;
        this.misc03 = misc03;
        this.misc04 = misc04;
        this.misc05 = misc05;
        this.misc06 = misc06;
        this.misc07 = misc07;
        this.misc08 = misc08;
        this.misc09 = misc09;
        this.misc10 = misc10;

        this.record =
        "Identification: " + id + "\n" +
        "Genealogy Index: " + genealogyIndex + "\n" +
        "Genealogy: " + genealogy + "\n" +
        "Name: " + name + "\n" +
        "Date Of Birth: " + dateOfBirth + "\n" +
        "Date Of Death: " + dateOfDeath + "\n" +
        "Spouse Name: " + spouseName + "\n" +
        "Spouse Date Of Birth: " + spouseDateOfBirth + "\n" +
        "Spouse Date Of Death: " + spouseDateOfDeath + "\n" +
        "Misc: " + misc01 + misc02 + misc03 + misc04 + misc05 + misc06 + misc07 + misc08 + misc09 + misc10;
    }

    public String getRecord() {
        return record;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGenealogyIndex() {
        return genealogyIndex;
    }

    public void setGenealogyIndex(String genealogyIndex) {
        this.genealogyIndex = genealogyIndex;
    }

    public String getGenealogy() {
        return genealogy;
    }

    public void setGenealogy(String genealogy) {
        this.genealogy = genealogy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(String dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getSpouseDateOfBirth() {
        return spouseDateOfBirth;
    }

    public void setSpouseDateOfBirth(String spouseDateOfBirth) {
        this.spouseDateOfBirth = spouseDateOfBirth;
    }

    public String getSpouseDateOfDeath() {
        return spouseDateOfDeath;
    }

    public void setSpouseDateOfDeath(String spouseDateOfDeath) {
        this.spouseDateOfDeath = spouseDateOfDeath;
    }

    public String getMisc() {
        return getMisc01() + " " + getMisc02() + " " + getMisc03() + " " + getMisc04() + " " + getMisc05() + " " +
                getMisc06() + " " + getMisc07() + " " + getMisc08() + " " + getMisc09() + " " + getMisc10();
    }

    public String getMisc01() {
        return misc01;
    }

    public void setMisc01(String misc01) {
        this.misc01 = misc01;
    }

    public String getMisc02() {
        return misc02;
    }

    public void setMisc02(String misc02) {
        this.misc02 = misc02;
    }

    public String getMisc03() {
        return misc03;
    }

    public void setMisc03(String misc03) {
        this.misc03 = misc03;
    }

    public String getMisc04() {
        return misc04;
    }

    public void setMisc04(String misc04) {
        this.misc04 = misc04;
    }

    public String getMisc05() {
        return misc05;
    }

    public void setMisc05(String misc05) {
        this.misc05 = misc05;
    }

    public String getMisc06() {
        return misc06;
    }

    public void setMisc06(String misc06) {
        this.misc06 = misc06;
    }

    public String getMisc07() {
        return misc07;
    }

    public void setMisc07(String misc07) {
        this.misc07 = misc07;
    }

    public String getMisc08() {
        return misc08;
    }

    public void setMisc08(String misc08) {
        this.misc08 = misc08;
    }

    public String getMisc09() {
        return misc09;
    }

    public void setMisc09(String misc09) {
        this.misc09 = misc09;
    }

    public String getMisc10() {
        return misc10;
    }

    public void setMisc10(String misc10) {
        this.misc10 = misc10;
    }
}
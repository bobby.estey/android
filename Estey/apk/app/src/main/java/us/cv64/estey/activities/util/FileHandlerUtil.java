package us.cv64.estey.activities.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import us.cv64.estey.Constants;

public class FileHandlerUtil {

    private Context context = null;

    /**
     * FileHandlerUtil Constructor
     * @param context
     */
    public FileHandlerUtil(Context context) {
        this.context = context;
    }

    /**
     * get all records in CSV file
     * @return
     */
    public String getAllRecords() {
        Log.i(Constants.TAG, "FileHandlerUtil.getAllRecords");
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        Scanner scanner = null;
        String line = "";
        String records = "";

        try {
            inputStream = assetManager.open(Constants.CSV_FILE);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            try {
                while ((line = bufferedReader.readLine()) != null) {
                    Log.i(Constants.TAG, "line: " + line);

                    scanner = new Scanner(line).useDelimiter("~");
                    records += scanner.next() + "\t";
                    records += scanner.next() + "\t";
                    records += scanner.next() + "\t";
                    records += scanner.next() + "\n";
                }

            } catch (Exception exception) {

            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return records;
    }

    /**
     * return records within an indexed range
     * @param startIndex
     * @param endIndex
     * @return
     */
    public String getRecordsRangeById(int startIndex, int endIndex) {
        Log.i(Constants.TAG, "FileHandlerUtil.getRecordsRangeById");
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        Scanner scanner = null;
        String line = "";
        String records = "";
        int currentIndex = 0;

        try {
            inputStream = assetManager.open(Constants.CSV_FILE);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            // skip header line
            bufferedReader.readLine();

            try {
                while ((line = bufferedReader.readLine()) != null) {
                    Log.i(Constants.TAG, "line: " + line);

                    scanner = new Scanner(line).useDelimiter("~");

                    currentIndex = Integer.parseInt(scanner.next());

                    if ((currentIndex >= startIndex) && (currentIndex <= endIndex)) {
                        records += Integer.toString(currentIndex) + "\t\t";
                        records += scanner.next() + "\t\t";
                        records += scanner.next() + "\t\t";
                        records += scanner.next() + "\n";
                    }
                }

            } catch (Exception exception) {
                exception.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return records;
    }
}

package us.cv64.estey.activities.genealogy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import us.cv64.estey.R;
import us.cv64.estey.Constants;
import us.cv64.estey.activities.ParentActivity;
/**
 * Genealogy Search Activity - displays information about an individual and contains navigation aids up and
 * down the Genealogy tree
 */
public class GenealogySearchActivity extends ParentActivity {
    
    private String identification = Constants.IDENTIFICATION_ROOT;
    private String index = Constants.GENEALOGY_INDEX_ROOT;
    private String genealogy = Constants.GENEALOGY_PARENT_ROOT;
    private EditText identificationValue = null;
    private EditText indexValue = null;
    private EditText genealogyValue = null;
    private EditText nameValue = null;
    private EditText dateOfBirthValue = null;
    private EditText dateOfDeathValue = null;
    private EditText spouceNameValue = null;
    private EditText spouceDOBValue = null;
    private EditText spouceDODValue = null;

    @Override
    /**
     * populate genealogy search with genealogy values
     */
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.TAG, "GenealogySearchActivity.onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genealogy_search);

        // get data from Genealogy Activity
        Bundle genealogyBundle = getIntent().getExtras();

        if (genealogyBundle == null) {
            return;
        }

        genealogy = genealogyBundle.getString("genealogy");
    }

    /**
     * clear fields on screen
     *
     * @param view
     */
    public void onClickClear(View view) {
        Log.i(Constants.TAG, "GenealogySearchActivity.onClickClear");

        genealogyValue = (EditText) findViewById(R.id.idGenealogyValue);
        genealogyValue.setText("");

        indexValue = (EditText) findViewById(R.id.idIndexValue);
        indexValue.setText("");

        identificationValue = (EditText) findViewById(R.id.idIdentificationValue);
        identificationValue.setText("");
    }

    /**
     * get records from the database
     *
     * @param view
     */
    public void onClickSearch(View view) {
        Log.i(Constants.TAG, "GenealogySearchActivity.onClickSearch");

        Intent intent = new Intent(this, GenealogyActivity.class);
        String toast = "";

        setupVariables();

        genealogy = genealogyValue.getText().toString().toUpperCase();
        index = indexValue.getText().toString().toUpperCase();
        identification = identificationValue.getText().toString().toUpperCase();

        if (!genealogy.isEmpty()) {
            toast = genealogy;
        } else if (!index.isEmpty()) {
            toast = index;
        } else if (!identification.isEmpty()) {
            toast = identification;
        }

        Toast.makeText(getApplicationContext(), "Searching for " + toast, Toast.LENGTH_SHORT).show();

        // pass the key / value to the Genealogy Activity
        intent.putExtra("genealogy", genealogy);
        intent.putExtra("index", index);
        intent.putExtra("identification", identification);

        // start the Genealogy activity
        startActivity(intent);
    }

    /**
     * set up genealogy search activity variables
     */
    private void setupVariables() {
        Log.i(Constants.TAG, "GenealogySearchActivity.setupVariables");

        identificationValue = (EditText) findViewById(R.id.idIdentificationValue);
        indexValue = (EditText) findViewById(R.id.idIndexValue);
        genealogyValue = (EditText) findViewById(R.id.idGenealogyValue);
        nameValue = (EditText) findViewById(R.id.idNameValue);
        dateOfBirthValue = (EditText) findViewById(R.id.idDateOfBirthValue);
        dateOfDeathValue = (EditText) findViewById(R.id.idDateOfDeathValue);
        spouceNameValue = (EditText) findViewById(R.id.idSpouceNameValue);
        spouceDOBValue = (EditText) findViewById(R.id.idSpouceDOBValue);
        spouceDODValue = (EditText) findViewById(R.id.idSpouceDODValue);
    }
}

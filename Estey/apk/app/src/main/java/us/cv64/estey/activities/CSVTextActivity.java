package us.cv64.estey.activities;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import us.cv64.estey.Constants;
import us.cv64.estey.R;
import us.cv64.estey.activities.util.FileHandlerUtil;

/**
 * CSV Text Activity
 */
public class CSVTextActivity extends ParentActivity {

    private TextView textView = null;
    private String csvRecords = "";

    @Override
    /**
     * create activity
     */
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.TAG, "CSVTextActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_csv_text);

        textView = (TextView) findViewById(R.id.csvTextActivityTextView);

        // get data from Main Activity
        Bundle mainActivityBundle = getIntent().getExtras();

        csvRecords = mainActivityBundle.getString("csvRecords");
        textView.setText(csvRecords);
    }

    /**
     * scroll results
     * @param view
     */
    public void onClickCSVTextViewScroll(View view) {
        Log.i(Constants.TAG, "CSVTextActivity.onClickCSVTextViewScroll");
        textView = (TextView) findViewById(R.id.csvTextActivityTextView);

        // textView.setText(csvRecords);

        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
    }

    /**
     * set the csv text field
     * @param view
     */
    public void onClickCSVTextSearchButton(View view) {
        Log.i(Constants.TAG, "CSVTextActivity.onClickCSVTextSearchButton");

        try {
            EditText csvSearchEditText = (EditText) findViewById(R.id.csvSearchEditText);
            String index = csvSearchEditText.getText().toString();

            if (index.isEmpty()) {
                index = Integer.toString(Constants.csvStartIndex);
            }

            int startIndex = Integer.parseInt(index);
            int endIndex = Integer.parseInt(index) + Constants.csvRecordRange;

            Toast.makeText(getApplicationContext(), "Records: " + startIndex + " - " + endIndex, Toast.LENGTH_SHORT).show();
            FileHandlerUtil fileHandlerUtil = new FileHandlerUtil(this);

            csvRecords = fileHandlerUtil.getRecordsRangeById(startIndex, endIndex);

            textView.setText(csvRecords);

        } catch (Exception exception) {
            Toast.makeText(getApplicationContext(), "Searching for index must be Integer input", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * clear the csv text field
     * @param view
     */
    public void onClickCSVTextClearButton(View view) {
        Log.i(Constants.TAG, "CSVTextActivity.onClickCSVTextClearButton");
        EditText csvSearchEditText = (EditText) findViewById(R.id.csvSearchEditText);

        csvSearchEditText.setText("");
    }
}
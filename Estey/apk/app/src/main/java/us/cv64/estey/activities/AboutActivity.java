package us.cv64.estey.activities;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.util.Log;

import us.cv64.estey.Constants;
import us.cv64.estey.R;
import us.cv64.estey.legal.License;

/**
 * About Estey Genealogy Program
 */
public class AboutActivity extends ParentActivity {
    
    License license = new License();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.TAG, "AboutActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView textView = (TextView) findViewById(R.id.idAboutTextViewScroll);
        textView.setText(license.getLicense());
    }

    // calls the legal content
    public void onClickTextViewScroll(View view) {
        Log.i(Constants.TAG, "onClickTextViewScroll.onClickCV64");
        TextView textView = (TextView) findViewById(R.id.idAboutTextViewScroll);
        textView.setText(license.getLicense());
        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
    }
}
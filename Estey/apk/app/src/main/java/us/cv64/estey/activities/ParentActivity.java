package us.cv64.estey.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.BufferedReader;

import us.cv64.estey.Constants;
import us.cv64.estey.R;
import us.cv64.estey.activities.util.ActionBarUtil;
import us.cv64.estey.activities.util.FileHandlerUtil;
import us.cv64.estey.sql.DatabaseHandler;

public class ParentActivity extends AppCompatActivity {

    protected DatabaseHandler databaseHandler = null;
    protected String csvRecords = "";

    /**
     * get a database handler object
     */
    protected DatabaseHandler getDatabaseHandler() {
        Log.i(Constants.TAG, "MainActivity.getDatabaseHandler");

        if (null == databaseHandler) {
            databaseHandler = DatabaseHandler.getInstance(this);
        }

        return databaseHandler;
    }

    /**
     * populate records from CSV Text file
     * @return CSV Text Records
     */
    protected String getCSVRecords() {
        Log.i(Constants.TAG, "MainActivity.getCSVRecords");
        BufferedReader bufferedReader = null;
        FileHandlerUtil fileHandlerUtil = new FileHandlerUtil(this);

        return fileHandlerUtil.getRecordsRangeById(Constants.csvStartIndex, Constants.csvEndIndex);
    }

    @Override
    /**
     * close out all objects
     */
    protected void onDestroy() {
        Log.i(Constants.TAG, "ParentActivity.destroy");

        super.onDestroy();

        if (null != databaseHandler) {
            databaseHandler.close();
            databaseHandler = null;
        }

        csvRecords = "";
    }

    @Override
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(Constants.TAG, "AboutActivity.onOptionsItemSelected");
        Intent intent = null;
        String actionBarString = new ActionBarUtil(item).getActionBarString();
        Toast.makeText(this, actionBarString, Toast.LENGTH_SHORT).show();

        if (actionBarString.equals("home")) {
            intent = new Intent(this, MainActivity.class);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(new ActionBarUtil(item).getActionBarLink()));
        }
        startActivity(intent);

        return super.onOptionsItemSelected(item);
    }

    @Override
    // Inflate the menu; this adds items to the action bar if it is present.
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(Constants.TAG, "AboutActivity.onCreateOptionsMenu");

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}

package us.cv64.constellation.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import us.cv64.constellation.Constants;
import us.cv64.constellation.R;

/**
 * Main Screen for the entire Application
 */
public class MainActivity extends ParentActivity {

    final private Context context = this;
    private ImageButton button = null;
    private AlertDialog.Builder alertDialogBuilder = null;
    private AlertDialog alertDialog = null;

    private String index = Constants.IDENTIFICATION_ROOT;

    @Override
    // launch the Intent Service when application starts
    //Intent intent = new Intent(this, EsteyIntentService.class);
    //startService(intent);
    protected void onCreate(Bundle savedInstconstellationeState) {
        Log.i(Constants.TAG, "MainActivity.onCreate");
        super.onCreate(savedInstconstellationeState);
        setContentView(R.layout.activity_main);

        /** keep this for future use somewhere else - gets the view's id
         final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
         .findViewById(android.R.id.content)).getChildAt(0);
         */
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * calls the coat of arms activity
     * @param view
     */
    public void onClickMapActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickMapActivity");

        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    /**
     * calls the contact activity
     *
     * @param view
     */
    public void onClickContactsActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickContactsActivity");

    }

    /**
     * calls the media activity
     * @param view
     */
    public void onClickMediaActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickMediaActivity");

        Intent intent = new Intent(this, MediaActivity.class);
        startActivity(intent);
    }

    /**
     *     calls the Questions activity
     */
    public void onClickQuestionsActivity(View view) {
        Log.i(Constants.TAG, "MainActivity.onClickQuestionsActivity");

        Intent intent = new Intent(this, QuestionsActivity.class);

        final TextView textView = (TextView) findViewById(R.id.aboutTextView);
        String userMessage = textView.getText().toString();

        // pass the key / value to the Questions Activity
        intent.putExtra("userMessage", userMessage);

        Log.i(Constants.TAG, "MainActivity.onClickQuestionsActivity.userMessage: " + userMessage);

        // start the Questions activity
        startActivity(intent);
    }
}
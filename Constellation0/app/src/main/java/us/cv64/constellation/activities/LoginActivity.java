package us.cv64.constellation.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import us.cv64.constellation.Constants;
import us.cv64.constellation.R;
import us.cv64.constellation.services.TimeService;

/**
 * Login Activity Class
 */
public class LoginActivity extends AppCompatActivity {

    private EditText username = null;
    private EditText password = null;
    private Button login = null;
    private TextView loginLockedTV = null;
    private TextView attemptsLeftTV = null;
    private TextView numberOfRemainingLoginAttemptsTV = null;
    private int numberOfRemainingLoginAttempts = Constants.LOGIN_ATTEMPTS;
    private TimeService timeService = null;
    private boolean isBound = false;
    private String currentTime = "";
    private NotificationCompat.Builder notification = null;
    private static final int UNIQUE_ID = 12345;

    /**
     * display the time stamp calling the time stamp service and notification information
     * @param view
     */
    public void onApplicationInformation(View view) {
        Log.i(Constants.TAG, "LoginActivity.onTimeStamp");

        // call the time service and display the current time
        if (null != timeService) {

            currentTime = timeService.getCurrentTime();

            Log.i(Constants.TAG, "LoginActivity.onTimeStamp - currentTime: " + currentTime);
        }

        // build the notification message
        if (null != notification) {

            notification.setSmallIcon(R.mipmap.constellation_icon);
            notification.setTicker(Constants.CONSTELLATION_TITLE + " : " + Constants.CONSTELLATION_PHONE);
            notification.setWhen(System.currentTimeMillis());
            notification.setContentTitle(Constants.CONSTELLATION_TITLE + " : " + Constants.CONSTELLATION_PHONE);
            notification.setContentText(Constants.CONSTELLATION_ADDRESS);

            // gives the device access to the inner apps, in other words brings up the application
            Intent intent = new Intent(this, LoginActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            notification.setContentIntent(pendingIntent);

            // build and send out the notification
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(UNIQUE_ID, notification.build());
        }
    }

    @Override
    /**
     * initializes login activity
     */
    protected void onCreate(Bundle savedInstconstellationeState) {
        Log.i(Constants.TAG, "LoginActivity.onCreate");

        super.onCreate(savedInstconstellationeState);
        setContentView(R.layout.activity_login);
        setupVariables();
        bind2TimeService();

        // create the notification
        notification = new NotificationCompat.Builder(this);

        // when a notification is read, remove from the ticker
        notification.setAutoCancel(true);
    }

    @Override
    /**
     *
     */
    protected void onResume() {
        Log.i(Constants.TAG, "LoginActivity.onResume");

        super.onResume();
    }

    /**
     * authenticates the login and navigates to the Main Activity on successful login
     *
     * @param view
     */
    public void authenticateLogin(View view) {
        Log.i(Constants.TAG, "LoginActivity.authenticateLogin");

        Intent intent = new Intent(this, MainActivity.class);

        // debug - start activity
        if (Constants.DEBUG) {

            // start the Main activity
            startActivity(intent);

            // force the user to login
        } else {

            if (username.getText().toString().equals(Constants.USERNAME) &&
                    password.getText().toString().equals(Constants.PASSWORD)) {

                Toast.makeText(getApplicationContext(), "Welcome " + username.getText(),
                        Toast.LENGTH_SHORT).show();

                intent = new Intent(this, MainActivity.class);

                // start the Main activity
                startActivity(intent);

            } else {
                Toast.makeText(getApplicationContext(), "Invalid Account",
                        Toast.LENGTH_SHORT).show();
                numberOfRemainingLoginAttempts--;
                attemptsLeftTV.setVisibility(View.VISIBLE);
                numberOfRemainingLoginAttemptsTV.setVisibility(View.VISIBLE);
                numberOfRemainingLoginAttemptsTV.setText(Integer.toString(numberOfRemainingLoginAttempts));

                if (numberOfRemainingLoginAttempts == 0) {
                    login.setEnabled(false);
                    loginLockedTV.setVisibility(View.VISIBLE);
                    loginLockedTV.setBackgroundColor(Color.RED);
                    loginLockedTV.setText("LOGIN LOCKED!!!");
                }
            }
        }
    }

    /**
     * set up login variables
     */
    private void setupVariables() {
        Log.i(Constants.TAG, "LoginActivity.setupVariables");

        username = (EditText) findViewById(R.id.usernameET);
        password = (EditText) findViewById(R.id.passwordET);
        login = (Button) findViewById(R.id.loginBtn);
        loginLockedTV = (TextView) findViewById(R.id.loginLockedTV);
        attemptsLeftTV = (TextView) findViewById(R.id.attemptsLeftTV);
        numberOfRemainingLoginAttemptsTV = (TextView) findViewById(R.id.numberOfRemainingLoginAttemptsTV);
        numberOfRemainingLoginAttemptsTV.setText(Integer.toString(numberOfRemainingLoginAttempts));
    }

    /**
     * configuring the Time Service
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            Log.i(Constants.TAG, "LoginActivity.onServiceConnected1");
            TimeService.LocalBinder localBinder = (TimeService.LocalBinder) service;
            Log.i(Constants.TAG, "LoginActivity.onServiceConnected2");
            timeService = localBinder.getService();
            Log.i(Constants.TAG, "LoginActivity.onServiceConnected3");
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            Log.i(Constants.TAG, "onServiceDisconnected");
            //timeService = null;
            isBound = false;
        }
    };

    /**
     * bind 2 Time Service
     */
    private void bind2TimeService() {
        Log.i(Constants.TAG, "LoginActivity.bind2TimeService - binding");

        Intent intent = new Intent(this, TimeService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

        Log.i(Constants.TAG, "LoginActivity.bind2TimeService - bound");
    }
}

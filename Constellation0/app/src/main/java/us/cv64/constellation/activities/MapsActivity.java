package us.cv64.constellation.activities;

import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import us.cv64.constellation.Constants;
import us.cv64.constellation.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final int MAX_RESULTS = 1;  // max return results on GEO queries
    private GoogleMap mMap = null;
    private double[] latitudeLongitude = new double[2];
    private String[] address = new String[5];

    @Override
    protected void onCreate(Bundle savedInstconstellationeState) {
        Log.i(Constants.TAG, "MapsActivity.onCreate");
        super.onCreate(savedInstconstellationeState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i(Constants.TAG, "MapsActivity.onMapReady");
        mMap = googleMap;

            //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID); // set in activity_maps.xml

        UiSettings mapSettings;
        mapSettings = mMap.getUiSettings();

        geocoding(Constants.CONSTELLATION_ADDRESS);

        LatLng constellation = new LatLng(latitudeLongitude[0], latitudeLongitude[1]);
        mMap.addMarker(new MarkerOptions().position(constellation).title(Constants.CONSTELLATION_TITLE + ":" + Constants.CONSTELLATION_PHONE));
        mMap.addMarker(new MarkerOptions().position(constellation).icon(BitmapDescriptorFactory.fromResource(R.mipmap.jon)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(constellation));

        reverseGeocoding(latitudeLongitude[0], latitudeLongitude[1]);
    }

    /**
     * geocoding - converting textual based geographical location (street address) into geographical coordinates
     *
     * @param address
     */
    public void geocoding(String address) {
        Log.i(Constants.TAG, "MapsActivity.geocoding");

        List<Address> geocodeMatches = null;

        for (int i = 0; i < latitudeLongitude.length; i++) {
            latitudeLongitude[i] = 0;
        }

        try {
            geocodeMatches = new Geocoder(this).getFromLocationName(address, MAX_RESULTS);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        if (!geocodeMatches.isEmpty()) {
            latitudeLongitude[0] = geocodeMatches.get(0).getLatitude();  // latitude
            latitudeLongitude[1] = geocodeMatches.get(0).getLongitude();  // longitude
        }

        Log.i(Constants.TAG, "MapsActivity.geocoding - coordinates: " + latitudeLongitude[0] + ":" + latitudeLongitude[1]);
    }

    /**
     * reverse geocoding - converting geographical coordinates into textual based geographical location (street address)
     *
     * @param latitude
     * @param longitude
     */
    public void reverseGeocoding(double latitude, double longitude) {
        Log.i(Constants.TAG, "MapsActivity.reverseGeocoding");

        List<Address> geocodeMatches = null;

        for (int i = 0; i < address.length; i++) {
            address[i] = "";
        }

        try {
            geocodeMatches = new Geocoder(this).getFromLocation(latitude, longitude, MAX_RESULTS);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        if (!geocodeMatches.isEmpty()) {
            address[0] = geocodeMatches.get(0).getAddressLine(0);
            address[1] = geocodeMatches.get(0).getAddressLine(1);
            address[2] = geocodeMatches.get(0).getAdminArea(); // state
            address[3] = geocodeMatches.get(0).getPostalCode(); // zip code
            address[4] = geocodeMatches.get(0).getCountryName(); // country
        }

        Log.i(Constants.TAG, "MapsActivity.reverseGeocoding - address: " +
                address[0] + ":" + address[1] + ":" + address[2] + ":" + address[3] + ":" + address[4]);
    }
}
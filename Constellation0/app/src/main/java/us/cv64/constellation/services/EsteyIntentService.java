package us.cv64.constellation.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import us.cv64.constellation.Constants;

public class EsteyIntentService extends IntentService {

    public EsteyIntentService() {

        super("EsteyIntentService");
    }

    @Override
    /**
     * this is what the service does
     */
    protected void onHandleIntent(Intent intent) {

        Log.i(Constants.TAG, "EsteyIntentService.onHandleIntent()");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(Constants.TAG, "EsteyIntentService.onStartCommand()");

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    long futureTime = System.currentTimeMillis() + 10000;
                    while (System.currentTimeMillis() < futureTime) {
                        synchronized (this) {
                            try {
                                wait(futureTime - System.currentTimeMillis());
                                Log.i(Constants.TAG, "Estey Intent Service is doing something");
                            } catch (Exception exception) {

                            }
                        }
                    }
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();

        // if the service ever gets destroyed, restart the service
        return this.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(Constants.TAG, "EsteyIntentService.onDestroy()");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
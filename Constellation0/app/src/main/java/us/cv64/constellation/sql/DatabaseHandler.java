package us.cv64.constellation.sql;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import us.cv64.constellation.Constants;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static DatabaseHandler databaseHandler = null;
    private static SQLiteDatabase database = null;

    public static synchronized DatabaseHandler getInstance(Context context) {
        Log.i(Constants.TAG, "DatabaseHandler Constructor getInstance");

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (databaseHandler == null) {
            databaseHandler = new DatabaseHandler(context);
        }

        return databaseHandler;
    }

    private DatabaseHandler(Context context) {
        super(context, Constants.CONSTELLATION_DATABASE, null, 1);
        Log.i(Constants.TAG, "DatabaseHandler Constructor");

        // Create and/or open a database that will be used for reading and writing
        database = getWritableDatabase();

        // if (database reset is true and database exists) or
        // if (database does not exist) - then reset
        if ((Constants.DATABASE_RESET == checkDatabase()) || (!checkDatabase())) {
            onUpgrade(database, Constants.DATABASE_OLD_VERSION, Constants.DATABASE_NEW_VERSION);
            readCSV(context);
        }
    }

    /**
     * close database
     */
    public void close() {
        database.close();
    }

    @Override
    /**
     * create table
     */
    public void onCreate(SQLiteDatabase database) {
        Log.i(Constants.TAG, "DatabaseHandler.onCreate");
        String columns = "";

        for (int i = 1; i < Constants.COLUMNS.length; i++) {
            columns += ", " + Constants.COLUMNS[i] + " TEXT ";
        }

        String query = "CREATE TABLE " + Constants.CONSTELLATION_TABLE + "(" +
                Constants.COLUMNS[0] + " TEXT PRIMARY KEY " +
                columns + ");";

        Log.i(Constants.TAG, "DatabaseHandler.onCreate - query: " + query);
        database.execSQL(query);
        Log.i(Constants.TAG, "DatabaseHandler.onCreate - table created: " + Constants.CONSTELLATION_TABLE);
    }

    @Override
    /**
     * drop table and create new table
     */
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.i(Constants.TAG, "DatabaseHandler.onUpgrade");
        String query = "DROP TABLE IF EXISTS " + Constants.CONSTELLATION_TABLE;
        Log.i(Constants.TAG, "DatabaseHandler.onUpgrade - query: " + query);
        database.execSQL(query);
        onCreate(database);
    }

    /**
     * insert record to the database
     * @param line
     */
    private void insertRecord(String line) {
        Log.i(Constants.TAG, "DatabaseHandler.insertRecord");
        ContentValues values = new ContentValues();

        String[] record = line.split(Constants.DELIMETER);

        Log.i(Constants.TAG, "DatabaseHandler.insertRecord: " + record[0] + ":" + record.length);

        for (int i = 0; i < Constants.COLUMNS.length; i++) {
            values.put(Constants.COLUMNS[i], record[i]);
        }

        database.insert(Constants.CONSTELLATION_TABLE, null, values);
    }

    /**
     * delete a record from the database
     * @param value
     */
    private void deleteRecord(int value) {
        Log.i(Constants.TAG, "DatabaseHandler.deleteRecord");
        String query = "DELETE FROM " + Constants.CONSTELLATION_TABLE + " WHERE " + Constants.COLUMNS[0] + "=\"" + value + "\";";

        Log.i(Constants.TAG, "DatabaseHandler.deleteRecord.query: " + query);
        database.execSQL(query);
    }

    /**
     * print out the database as a string
     */
    private void databaseToString() {
        Log.i(Constants.TAG, "DatabaseHandler.databaseToString");

        String query = "SELECT * FROM " + Constants.CONSTELLATION_TABLE;
        Log.i(Constants.TAG, "DatabaseHandler.databaseToString.query: " + query);
        // cursor point to a location in result
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();

        Log.i(Constants.TAG, "DatabaseHandler.databaseToString.cursor.count: " + cursor.getCount());
        for (int j = 0; j < cursor.getCount(); j++) {
            for (int i = 1; i < Constants.COLUMNS.length; i++) {
                Log.i(Constants.TAG, "DatabaseHandler.databaseToString.cursor.columnName: " + cursor.getColumnName(i));
            }
        }
    }

    /**
     * read CSV
     * @param context
     */
    private void readCSV(Context context) {
        Log.i(Constants.TAG, "DatabaseHandler.readCSV");
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;

        String line = "";

        BufferedReader bufferedReader = null;

        try {
            inputStream = assetManager.open(Constants.CONSTELLATION_CSV_FILE);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = bufferedReader.readLine()) != null) {
                Log.i(Constants.TAG, "line: " + line);
                insertRecord(line);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            Log.i(Constants.TAG, "EXCEPTION LINE: " + line);
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        }
    }

    /**
     * @return if database exists
     */
    private boolean checkDatabase() {
        Log.i(Constants.TAG, "DatabaseHandler.checkDatabase");

        try {
            String query = "SELECT * FROM " + Constants.CONSTELLATION_TABLE;
            Log.i(Constants.TAG, "DatabaseHandler.checkDatabase.query: " + query);
            // cursor point to a location in result
            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            Log.i(Constants.TAG, "DatabaseHandler.checkDatabase.moveToFirst: " + cursor.getCount());

            if (cursor.getCount() > 0) {
                Log.i(Constants.TAG, "DatabaseHandler.checkDatabase: true ");
                return true;
            } else {
                Log.i(Constants.TAG, "DatabaseHandler.checkDatabase: false");
                return false;
            }
        } catch (Exception exception) {
            exception.getMessage();
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * get record
     * @param field
     * @param value
     * @return
     */
    public EsteyRecord getRecord(String field, String value) {
        Log.i(Constants.TAG, "DatabaseHandler.getRecord");
        EsteyRecord esteyRecord = null;

        try {
            database = getWritableDatabase();

            String query = "SELECT * FROM " + Constants.CONSTELLATION_TABLE + " WHERE " + field + " = '" + value + "'";
            Log.i(Constants.TAG, "DatabaseHandler.getRecord.query: " + query);
            // cursor point to a location in result
            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            Log.i(Constants.TAG, "DatabaseHandler.getRecord.moveToFirst: " + cursor.getCount());

            if (cursor.getCount() > 0) {
                Log.i(Constants.TAG, "DatabaseHandler.getRecord: true ");
                esteyRecord = new EsteyRecord(cursor);

                Log.i(Constants.TAG, "DatabaseHandler.getRecord - esteyRecord.getContacts: " + esteyRecord.getContacts());
                return esteyRecord;
            } else {
                Log.i(Constants.TAG, "DatabaseHandler.getRecord: false");
                return null;
            }
        } catch (Exception exception) {
            exception.getMessage();
            exception.printStackTrace();
            return null;
        } finally {
            database.close();
        }
    }

    /**
     * get records
     *
     * @param genealogy
     * @return
     */
    public ArrayList<EsteyRecord> getRecords(String genealogy) {
        Log.i(Constants.TAG, "DatabaseHandler.getRecord");

        ArrayList<EsteyRecord> esteyArrayList = new ArrayList<EsteyRecord>();
        try {
            database = getWritableDatabase();

            String query = "SELECT * FROM " + Constants.CONSTELLATION_TABLE + " WHERE GENEALOGY LIKE '" + genealogy + "_'";
            Log.i(Constants.TAG, "DatabaseHandler.getRecord.query: " + query);
            // cursor point to a location in result
            Cursor cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            Log.i(Constants.TAG, "DatabaseHandler.getRecord.moveToFirst: " + cursor.getCount());

            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    Log.i(Constants.TAG, "DatabaseHandler.getRecord: true ");
                    esteyArrayList.add(new EsteyRecord(cursor));
                    cursor.moveToNext();

                    Log.i(Constants.TAG, "DatabaseHandler.getRecord - esteyRecord.getContacts: " + esteyArrayList.get(0));
                }
                return esteyArrayList;

            } else {
                Log.i(Constants.TAG, "DatabaseHandler.getRecord: false");
                return null;
            }

        } catch (Exception exception) {
            exception.getMessage();
            exception.printStackTrace();
            return null;
        } finally {
            database.close();
        }
    }

    /**
     * print records
     *
     * @param record
     */
    private void printRecord(String[] record) {
        Log.i(Constants.TAG, "DatabaseHandler.printRecord ");
        Log.i(Constants.TAG, Integer.toString(record.length));

        for (int i = 0; i < record.length; i++) {
            Log.i(Constants.TAG, "[" + i + "]: " + record[i]);
        }
    }
}
package us.cv64.constellation.activities.util;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.support.v4.view.GestureDetectorCompat;

import us.cv64.constellation.Constants;
import us.cv64.constellation.R;
import us.cv64.constellation.activities.ParentActivity;

public class GestureUtil extends ParentActivity implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    private TextView idEsteyMessage;
    private GestureDetectorCompat gestureDetectorCompat;

    @Override
    protected void onCreate(Bundle savedInstconstellationeState) {
        Log.i(Constants.TAG, "GesterUtil.onCreate");
        super.onCreate(savedInstconstellationeState);
        setContentView(R.layout.activity_main);

        // get the id of the message
        idEsteyMessage = (TextView) findViewById(R.id.idEsteyMessage);

        // detect gesters
        this.gestureDetectorCompat = new GestureDetectorCompat(this, this);

        // detect double taps on the activity
        gestureDetectorCompat.setOnDoubleTapListener(this);
    }

    // BEGIN GESTERS ********************
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        Log.i(Constants.TAG, "GesterUtil.onSingleTapConfirmed");
        idEsteyMessage.setText("onSingleTapConfirmed");
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        Log.i(Constants.TAG, "GesterUtil.onDoubleTap");
        idEsteyMessage.setText("onDoubleTap");
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        Log.i(Constants.TAG, "GesterUtil.onDoubleTapEvent");
        idEsteyMessage.setText("onDoubleTapEvent");
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        Log.i(Constants.TAG, "GesterUtil.onDown");
        idEsteyMessage.setText("onDown");
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        Log.i(Constants.TAG, "GesterUtil.onShowPress");
        idEsteyMessage.setText("onShowPress");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        Log.i(Constants.TAG, "GesterUtil.onSingleTapUp");
        idEsteyMessage.setText("onSingleTapUp");
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distconstellationeX, float distconstellationeY) {
        Log.i(Constants.TAG, "GesterUtil.onScroll");
        idEsteyMessage.setText("onScroll");
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        Log.i(Constants.TAG, "GesterUtil.onLongPress");
        idEsteyMessage.setText("onLongPress");
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Log.i(Constants.TAG, "GesterUtil.onFling");
        idEsteyMessage.setText("onFling");
        return true;
    }

    // END GESTERS ********************

    // on default looks for when the user touch the activity
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.i(Constants.TAG, "GesterUtil.onTouchEvent");
        // detect if a gester otherwise continue
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(Constants.TAG, "GesterUtil.onCreateOptionsMenu");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(Constants.TAG, "GesterUtil.onOptionsItemSelected");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


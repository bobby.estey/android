package us.cv64.constellation;

public class Constants {
    public static final boolean DEBUG = true;  // true - bypass password login
    public static final boolean DATABASE_RESET = false;  // true - rebuild database
    public static final int DATABASE_OLD_VERSION = 1;
    public static final int DATABASE_NEW_VERSION = 1;

    public static final String VERSION_FULL = "CONSTELLATION Glass - v20151219 (Jon)";
    public static final String VERSION = "v20151219 (Jon)";
    public static final String CONSTELLATION_TITLE = "CONSTELLATION Glass";
    public static final String CONSTELLATION_WEBSITE = "constellationglass.com";
    public static final String CONSTELLATION_ADDRESS = "6870 Broadway, Unit J, Denver, CO 80221";
    public static final String CONSTELLATION_PHONE = "303.697.9712";

    public static final int LOGIN_ATTEMPTS = 3;
    public static final String USERNAME = "constellation";
    public static final String PASSWORD = "walter";

    public static final int ACTIVITY_REQUEST_CODE = 1;
    public static final String TAG = "us.cv64.constellation";
    public static final String IDENTIFICATION_ROOT = "1";

    public static final String NEBRASKA_WHITE = "#f5f1e7"; // Nebraska WHITE
    public static final String NEBRASKA_RED = "#d00000"; // Nebraska RED

    public static final String DELIMETER = "~"; // database delimeter
    public static final String CONSTELLATION_CSV_FILE = "constellation20151219.csv";  // located in project assets directory
    public static final String CONSTELLATION_DATABASE = "constellation.database";
    public static final String CONSTELLATION_TABLE = "constellation";

    public static final String JOHN = "1";
    public static final String ROB = "2";

    public static final String CV64 = "http://www.rebekahestey.com/cv64/";
    public static final String CV64_VERSION = "http://www.rebekahestey.com/cv64/estey/constellation/version";
    public static final String CONSTELLATION_LINKS = "Links"; // Android Overflow Widget with Icons
    public static final String CONSTELLATION_FACEBOOK_TITLE = "CONSTELLATION Facebook";
    public static final String CONSTELLATION_FACEBOOK_LINK = "https://www.facebook.com/groups/constellation/constellation";
    public static final String MEDIA = "http://www.rebekahestey.com/cv64/estey/constellation/media";
    public static final String VIDEO = "http://www.rebekahestey.com/cv64/estey/constellation/media/huskers1.mp4";

    public static final String[] COLUMNS = {
            "IDENTIFICATION", "NAME", "PHONE", "ADDRESS"};
}

